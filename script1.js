function showSidebar() {
    const sidebar = document.querySelector('.nav_links-sidebar');
    sidebar.style.display = "flex";
}

function hideSidebar() {
    const sidebar = document.querySelector('.nav_links-sidebar');
    sidebar.style.display = "none";
}

class Chatbox {
    constructor() {
        this.args = {
            openButton: document.querySelector('.chatbox__button'),
            micButton: document.querySelector('.mic__button'),
            chatBox: document.querySelector('.chatbox__support'),
            sendButton: document.querySelector('.send__button')
        };

        this.state = false;
        this.messages = [];
        this.responses = {}; // Object to store responses from intents1.json
    }

    // Load responses from intents1.json
    async loadResponses() {
        try {
            const response = await fetch('intents1.json'); // Assuming intents1.json is in the same directory
            this.responses = await response.json();
        } catch (error) {
            console.error('Error loading responses:', error);
        }
    }

    // Function to preprocess text
    preprocessText(inputText) {
        // Remove question marks, exclamation points, and other punctuation
        return inputText.replace(/[?!\.,;:'"]/g, '');
    }

    async display() {
        await this.loadResponses(); // Load responses before displaying

        const { openButton, micButton, chatBox, sendButton } = this.args;

        openButton.addEventListener('click', () => this.toggleState(chatBox));
        micButton.addEventListener('click', () => this.onMicButton(chatBox));
        sendButton.addEventListener('click', () => this.onSendButton(chatBox));

        const node = chatBox.querySelector('input');
        node.addEventListener('keyup', ({ key }) => {
            if (key === 'Enter') {
                this.onSendButton(chatBox);
            }
        });
    }

    toggleState(chatBox) {
        this.state = !this.state;

        if (this.state) {
            chatBox.classList.add('chatbox--active');
        } else {
            chatBox.classList.remove('chatbox--active');
        }
    }

    onMicButton(chatBox) {
        const recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition)();
        recognition.lang = 'en-US';
        recognition.interimResults = false;
        recognition.maxAlternatives = 1;

        recognition.start();

        recognition.onresult = (event) => {
            const text = event.results[0][0].transcript;
            this.processAndSendMessage(chatBox, text);
        };

        recognition.onerror = (event) => {
            console.error('Speech recognition error:', event.error);
        };

        recognition.onend = () => {
            recognition.stop();
        };
    }

    onSendButton(chatBox) {
        const textField = chatBox.querySelector('input');
        const text = textField.value;

        if (text === '') {
            return;
        }

        this.processAndSendMessage(chatBox, text);
        textField.value = '';
    }

    processAndSendMessage(chatBox, inputText) {
        const processedText = this.preprocessText(inputText);
        const msg = { name: 'User', message: processedText };
        this.messages.push(msg);

        const intent = this.determineIntent(processedText);
        const response = this.getRandomResponse(intent);
        const msg2 = { name: 'Sam', message: response };
        this.messages.push(msg2);

        this.updateChatText(chatBox);
    }

    determineIntent(text) {
        const lowerText = text.toLowerCase();
        for (const intent of this.responses.intents) {
            for (const pattern of intent.patterns) {
                const cleanPattern = pattern.replace(/[^\w\s]/gi, '').toLowerCase();
                if (lowerText.includes(cleanPattern)) {
                    return intent;
                }
            }
        }
        return null;
    }
    

    getRandomResponse(intent) {
        if (intent && intent.responses.length > 0) {
            const randomIndex = Math.floor(Math.random() * intent.responses.length);
            return intent.responses[randomIndex];
        }
        return 'I am not sure how to respond to that.';
    }

    updateChatText(chatBox) {
        let html = '';
        this.messages.slice().reverse().forEach(function (item) {
            if (item.name === 'Sam') {
                html += '<div class="messages__item messages__item--visitor">' + item.message + '</div>';
            } else {
                html += '<div class="messages__item messages__item--operator">' + item.message + '</div>';
            }
        });
        const chatMessages = chatBox.querySelector('.chatbox__messages');
        chatMessages.innerHTML = html;
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const chatbox = new Chatbox();
    chatbox.display();
});
